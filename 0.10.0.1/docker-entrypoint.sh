#!/bin/bash
set -e

if [ "$1" = 'kafka' ]; then

	: ${KAFKA_HOME="/opt/kafka"}
	: ${KAFKA_HOST="$(hostname --ip-address)"}
	: ${KAFKA_ADVERTISED_HOST=${KAFKA_HOST}}
	: ${KAFKA_PORT="9092"}
	: ${KAFKA_ADVERTISED_PORT=${KAFKA_PORT}}
	: ${KAFKA_LOG_DIR="${KAFKA_HOME}/logs"}
	: ${KAFKA_RETENTION_HOURS="168"}
	: ${KAFKA_ROLL_HOURS="${KAFKA_RETENTION_HOURS}"}

	: ${KAFKA_ZK="`echo $(dig +short master.mesos) | sed 's|\ |:2181,|g'`:2181/kafka"}

	: ${KAFKA_HEAP="none"}
	if [ "${KAFKA_HEAP}" = "none" ]; then
        KAFKA_HEAP=`/calculate-heap-limit.sh`
	fi

	if [ "${KAFKA_HEAP}" != "default" ]; then
        export KAFKA_HEAP_OPTS="-Xms${KAFKA_HEAP} -Xmx${KAFKA_HEAP}"
    fi

	KAFKA_CONF=${KAFKA_HOME}/config/docker-server.properties

	echo "host.name=${KAFKA_HOST}" >> ${KAFKA_CONF}
	echo "port=${KAFKA_PORT}" >> ${KAFKA_CONF}
	echo "advertised.host.name=${KAFKA_ADVERTISED_HOST}" >> ${KAFKA_CONF}
	echo "advertised.port=${KAFKA_ADVERTISED_PORT}" >> ${KAFKA_CONF}
	echo "log.dir=${KAFKA_LOG_DIR}" >> ${KAFKA_CONF}
	echo "log.retention.hours=${KAFKA_RETENTION_HOURS}" >> ${KAFKA_CONF}
	echo "log.roll.hours=${KAFKA_ROLL_HOURS}" >> ${KAFKA_CONF}
	echo "zookeeper.connect=${KAFKA_ZK}" >> ${KAFKA_CONF}

	echo "KAFKA_HOST: ${KAFKA_HOST}"
	echo "KAFKA_PORT: ${KAFKA_PORT}"
	echo "KAFKA_ADVERTISED_HOST: ${KAFKA_ADVERTISED_HOST}"
	echo "KAFKA_ADVERTISED_PORT: ${KAFKA_ADVERTISED_PORT}"
    echo "KAFKA_ZK: ${KAFKA_ZK}"
    echo "KAFKA_HEAP: ${KAFKA_HEAP}"

    mkdir -p ${KAFKA_LOG_DIR}
    chown -R kafka ${KAFKA_HOME}

	exec gosu kafka ${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_CONF}

fi

exec "$@"
