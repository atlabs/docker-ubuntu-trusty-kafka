FROM atlp/ubuntu-trusty-java:jre8
MAINTAINER Allied Telesis

# explicitly set user/group IDs
RUN groupadd -r kafka --gid=999 && useradd -r -g kafka --uid=999 kafka

ENV KAFKA_VERSION="0.10.0.1"
ENV SCALA_VERSION="2.11"
ENV KAFKA_HOME="/opt/kafka"

RUN mkdir -p /opt && \
    wget -nv http://www-us.apache.org/dist/kafka/$KAFKA_VERSION/kafka_$SCALA_VERSION-$KAFKA_VERSION.tgz && \
    tar -zxvf kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz && \
    mv kafka_${SCALA_VERSION}-${KAFKA_VERSION} $KAFKA_HOME && \
    rm kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz
RUN mkdir -p $KAFKA_HOME/logs

ADD http://central.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.7/jmx_prometheus_javaagent-0.7.jar $KAFKA_HOME/bin/jmx_prometheus_javaagent.jar
COPY prometheus-config.yml $KAFKA_HOME/config
ENV KAFKA_OPTS="-javaagent:$KAFKA_HOME/bin/jmx_prometheus_javaagent.jar=${METRICS_PORT:-9091}:$KAFKA_HOME/config/prometheus-config.yml"

RUN chown -R kafka /opt

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["kafka"]