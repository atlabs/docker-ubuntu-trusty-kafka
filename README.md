# docker-cassandra-mesos
A docker build to run Kafka on Marathon.


# Building and Pushing

Create this image as follows:

	sudo docker build -t atlp/kafka:0.10.0.1 0.10.0.1/.

Now that our image is built, push it to your registry:

	sudo docker push atlp/kafka:0.10.0.1


# Environment Variables

## KAFKA_HOST

Sets the host IP number to which to bind:

	$ docker run -it --net host -e KAFKA_HOST=10.10.25.14 atlp/kafka:0.10.0.1
	 
If no `KAFKA_HOST` is set, the IP number associated with the hostname will be used.	 

## KAFKA_PORT

The port ot bind to, by default 9092

## KAFKA_ADVERTISED_HOST

When using docker bridged networking, you may need to set this value to the external IP number on which the dockerized
kafka service can be reached.

## KAFKA_ADVERTISED_PORT

The port to which external service can bind to the dockerized kafka service.

## KAFKA_LOG_DIR

Sets the location where Kafka will store its logs. Default /opt/kafka/logs

## KAFKA_RETENTION_HOURS

Sets the number of hours Kafka will keep its logs. Default is 168 hours (7 days).

## KAFKA_ROLL_HOURS

Sets the number of hours before a new log is started. Defaults to the value set
for `KAFKA_RETENTION_HOURS`.

## KAFKA_ZK

The url to the Kafka Zookeeper. Should look something like `cluster1:2181,cluster2:2181,cluster3:2181/kafka`. If
not set, this value is determined by resolving the dns name `master.mesos`.

## KAFKA_HEAP

The heap size used for Kafka. Default is 0.75 of available memory for the container. If
you set this manually, make sure to not exceed with a reasonable margin the container 
memory!  

## METRICS_PORT

This Kafka server is started with the Prometheus JMX Exporter. The port from which you can access the metrics is defined
through this variable, and is by default set to port `9091`:

	$ docker run -it -p 9092:9092 -p 9091:9091 -e METRICS_PORT=9091 atlp/kafka:0.10.0.1	





